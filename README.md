# Insta Block
Use **hook_preprocess_insta_block**, to change the data that comes into the template.
## The contents of the variable _hook_preprocess_insta_block_:
```php
    'data' => [
        'posts'. // post data like: image, post link, uri saved file
        'profile'=> [
            'url', // link to profile
            'id',
            'username'.
        ],
    ];
    
    'settings'  // Settigns
    'insta_attr' // Attributes
```

**The template contains** 
```twig
{{ insta_prefix }}
{# post template #}
{{ insta_suffix }}
```
