<?php

namespace Drupal\insta_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\Client;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\RequestException;
use Drupal\image\Entity\ImageStyle;
use Drupal\Component\Utility\Unicode;

/**
* Provides the instagram block
*
* @Block(
*   id = "instagram_block",
*   admin_label = @Translation("Instagram Block"),
*   category = @Translation("Instagram Block")
* )
*
*/
class InstaBlock extends BlockBase implements ContainerFactoryPluginInterface{

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;


  /**
   * The config factory
   *
   * @var \Drupal\Core\Config\ConfigFactory
   *
   */
  protected $configFactory;

  /**
   * Constructs a InstagramBlockBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\Client $http_client
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The factory for configuration objects.
   */

  public function __construct(array $configuration, $plugin_id, array $plugin_definition, Client $http_client, ConfigFactory $config_factory) {
      parent::__construct($configuration, $plugin_id, $plugin_definition);
      $this->httpClient    = $http_client;
      $this->configFactory = $config_factory;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
      return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('http_client'),
          $container->get('config.factory')
      );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
      return array(
          'user_name'         => '',
          'image_style'       => 'thumbnail',
          'count'             => 4,
          'post_info'         => 1,
          'link_class'        => 'insta-link',
          'wrapper_class'     => 'insta-row',
          'use_link'          => 0,
          'full_image'        => 1,
          'class_container'   => 'insta-views',
          'lazy_load'         => 0,
          'to_post'           => 0
      );
  }

  public function getStylesImage() {
      $image_style = array();
      $site_styles = ImageStyle::loadMultiple();

      foreach( $site_styles as $key=>$style ) {
          $image_style[$key] = $key;
      }
      return $image_style;
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

      $form['basic'] = array(
          '#type' => 'details',
          '#title' => $this->t('Basic settings'),
          '#attributes' => array(
              'open' => true
          ),
      );

      $form['basic']['user_name'] = array(
          '#type'             => 'textfield',
          '#title'            => $this->t('User Name'),
          '#description'      => $this->t('Your Instagram user name.'),
          '#default_value'    => $this->configuration['user_name'],
          '#required'         => TRUE
      );

      $form['basic']['image_style'] = array(
          '#type'             => 'select',
          '#title'            => $this->t('Image Style'),
          '#description'      => $this->t('Choose a style for images. Please note that original images are only sized 640x480.'),
          '#default_value'    => $this->configuration['image_style'],
          '#options'          => $this->getStylesImage(),
      );


      $form['basic']['count'] = array(
          '#type'             => 'number',
          '#title'            => $this->t('Number of images to display'),
          '#default_value'    => $this->configuration['count'],
          '#attributes'       => array(
              'min' => '1',
              'max' => '12'
          ),
      );

      $form['additional'] = array(
          '#type' => 'details',
          '#title' => $this->t('Additional settings'),
      );

      $form['additional']['post_info'] = array(
          '#type'             => 'checkbox',
          '#title'            => $this->t('Information post'),
          '#description'      => $this->t('Provides information about the post how comment count and likes'),
          '#default_value'    => $this->configuration['post_info'],
      );

      $form['additional']['lazy_load'] = array(
          '#type'             => 'checkbox',
          '#title'            => $this->t('Attr for lazy load'),
          '#description'      => $this->t('Provides attribute for lazy load'),
          '#default_value'    => $this->configuration['lazy_load'],
      );

      $form['output'] = array(
          '#type' => 'details',
          '#title' => $this->t('Styling output'),
      );
      $form['output']['wrapper_element'] = array(
          '#type' => 'details',
          '#title' => $this->t('Wrapper element'),
      );

      $form['output']['wrapper_element']['class_container'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Class container'),
          '#description' => $this->t('Allows you to specify a class for a container'),
          '#default_value' => $this->configuration['class_container'],
      );
      $form['output']['wrapper_element']['wrapper_class'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Class wrapper div'),
          '#description' => $this->t('Allows you to specify a class for a wrapper'),
          '#default_value' => $this->configuration['wrapper_class'],
      );

      $form['output']['link_attributes'] = array(
          '#type' => 'details',
          '#title' => $this->t('Wrapper link'),
      );

      $form['output']['link_attributes']['link_class'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Class wrapper link'),
          '#description' => $this->t('Allows you to specify a class for a link'),
          '#default_value' => $this->configuration['link_class'],
      );

      $form['output']['link_attributes']['use_link'] = array(
          '#type' => 'checkbox',
          '#title' => $this->t('Use wrapper link'),
          '#description' => $this->t('Rotate the image to link to the original content or full image'),
          '#default_value' => $this->configuration['use_link'],
      );

      $form['output']['link_attributes']['full_image'] = array(
          '#type' => 'checkbox',
          '#title' => $this->t('Full image'),
          '#description' => $this->t('Adds a date attribute with a link to the full image size'),
          '#default_value' => $this->configuration['full_image'],
      );

      $form['output']['link_attributes']['to_post'] = array(
          '#type' => 'checkbox',
          '#title' => $this->t('Open instagram post'),
          '#description' => $this->t('Go to instagram feed with this post'),
          '#default_value' => $this->configuration['to_post'],
      );

      return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
      if( $form_state->hasAnyErrors() ) {
          return;
      } else {
          $this->configuration['user_name']         = $form_state->getValue('basic')['user_name'];
          $this->configuration['image_style']       = $form_state->getValue('basic')['image_style'];
          $this->configuration['count']             = $form_state->getValue('basic')['count'];
          $this->configuration['post_info']         = $form_state->getValue('additional')['post_info'];
          $this->configuration['lazy_load']         = $form_state->getValue('additional')['lazy_load'];
          $this->configuration['class_container']   = $form_state->getValue('output')['wrapper_element']['class_container'];
          $this->configuration['wrapper_class']     = $form_state->getValue('output')['wrapper_element']['wrapper_class'];
          $this->configuration['link_class']        = $form_state->getValue('output')['link_attributes']['link_class'];
          $this->configuration['use_link']          = $form_state->getValue('output')['link_attributes']['use_link'];
          $this->configuration['full_image']        = $form_state->getValue('output')['link_attributes']['full_image'];
          $this->configuration['to_post']           = $form_state->getValue('output')['link_attributes']['to_post'];
      }
  }

  /**
   * {@inheritDoc}
   */
      public function build() {
      // Build a render array to return the Instagram Images.
      $build = array();

      $url = "https://www.instagram.com/{$this->configuration['user_name']}";
  
      // Get Response file_get_contents($url);
      $response =  $this->fetchData( $url );
      if(!$response) {
          return [
              '#markup' => "<div class='view-empty'>".t('Sorry, but you did not post any posts on your page instagram')."</div>",
          ];
      };
  
      // The start position.
      $start_position = strpos($response, 'window._sharedData = ');
      // String length to trim before.
      $start_positionlength = strlen('window._sharedData = ');
      // Trim preceding content.
      $trimmed_before = trim(substr($response, ($start_position + $start_positionlength)));
      // End position.
      $end_position = strpos($trimmed_before, '</script>');
      // Trim content.
      $trimmed = trim(substr($trimmed_before, 0, $end_position));
      // Remove extra trailing ";".
      $jsondata = substr($trimmed, 0, -1);
      // JSON decode.
      $obj = Json::decode($jsondata, TRUE);

      // If the profile private.
      if ($obj['entry_data']['ProfilePage']['0']['graphql']['user']['is_private']) {
          return [
              '#markup' => "<div class='view-empty'>".t('Sorry, but you did not post any posts on your page instagram')."</div>",
          ];
      }

      if (isset($obj['entry_data']['ProfilePage']['0']['graphql']['user']['edge_owner_to_timeline_media']['edges'])) {
      $variable = $obj['entry_data']['ProfilePage']['0']['graphql']['user']['edge_owner_to_timeline_media']['edges'];

      // Get only the image Post.
      foreach ($variable as $key=>$value) {
          if (!$value['node']['is_video']) {
              $image_post[] = $value;
          }
      }
      
      // in develop
      // if (count($image_post) < 4) {
      //     foreach ($variable as $value) {
      //     if ($value['node']['is_video']) {
      //         $image_post[] = $value;
      //     }
      //     }
      // }

      $slice_variable = array_slice($image_post, 0, $this->configuration['count']);

      foreach ($slice_variable as $value) {
          // Generate path.
          $shortcode = $value['node']['shortcode'];
          
          if (isset($value['node']['edge_media_to_caption']['edges'][0]['node']['text'])) {
              $caption = $value['node']['edge_media_to_caption']['edges'][0]['node']['text'];
          }
          else {
              $caption = '';
          }
          $trim_caption = Unicode::truncate($caption, 30, '...');

          // Generate style
          $style = ImageStyle::load($this->configuration['image_style']);

          // Image source.
          $src = $value['node']['display_url'];

          // Get path with external image
          $scr = imagecache_external_generate_path($src);
          $data['posts'][] = [
              'original_image'  => $src,
              'preset'          => $style->buildUrl($scr),
              'uri'             => $scr,
              'caption'         => $trim_caption,
              'comment_count'   => !empty($value['node']['edge_media_to_comment']['count']) ? $value['node']['edge_media_to_comment']['count'] : '',
              'like_count'      => !empty($value['node']['edge_liked_by']['count']) ? $value['node']['edge_liked_by']['count'] : '',
              'insta_path'      => 'https://www.instagram.com/p/' . $shortcode,
          ];
      }

      $data['profile'] = [
          'url'       =>  $url,
          'id'        =>  $slice_variable[0]['node']['owner']['id'],
          'username'  =>  $slice_variable[0]['node']['owner']['username'],
      ];

      $build = [
          '#theme'        => 'insta_block',
          '#data'         => $data,
          '#settings'     => [
              'use_link'     => $this->configuration['use_link'],
              'full_image'   => $this->configuration['full_image'],
              'lazy_load'    => $this->configuration['lazy_load'],
              'to_post'      => $this->configuration['to_post']
          ],
          '#post_info'    => $this->configuration['post_info'],
          '#insta_attr'   => [
              'class_container'   =>  strip_tags( strtolower($this->configuration['class_container']) ) ,
              'wrapper'           =>  strip_tags( strtolower($this->configuration['wrapper_class']) ) ,
              'link'              =>  strip_tags( strtolower($this->configuration['link_class']) ),
          ],
      ];
          
  }

  // Cache for a day.
  $build['#cache'] = [
      'max-age' => 36000,
      'keys' => [
          'block',
          'instagram_block',
          $this->configuration['id'],
          $this->configuration['user_name'],
      ],
  ];

  return $build;
}

  /**
   * Get Response file_get_contents($url);
  * @param  string $media_uri
  * URL for http request.
  * @return bool|mixed
  * The encoded response containing them instagram images or FALSE
  */
  protected function fetchData($url) {
      try {
          if ($cache = \Drupal::cache()->get('instagram_block_data:' . $url)) {
              $data = $cache->data;
          } else {
              $instagram_url = $this->httpClient->request('GET', $url);
              $data = (string) $instagram_url->getBody();

              \Drupal::cache()->set('instagram_block_data:' . $url, $data);
          }
          if( empty( $data ) ) {
              return FALSE;
          }

          return $data;
      } catch ( RequestException $e ) {
          return FALSE;
      }
  }
}
